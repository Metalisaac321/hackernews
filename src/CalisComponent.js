import React, { Component } from 'react'

class CalisComponent extends Component {
  constructor() {
    super();
    this.onClickMe = this.onClickMe.bind(this);
    this.state = {
      secreto: 'Este es un secreto'
    }
  }

  onClickMe() {
    console.log(this);
  }

  render() {
    return (
      <button
        onClick={this.onClickMe}
        type="button">
        Click Me
    </button>);
  }
}
export default CalisComponent;
